FROM ubuntu:14.04

MAINTAINER Diego Garcia <drgarcia1986@gmal.com>

ADD requirements.txt /var/www/encolinks/requirements.txt
ADD run.py /var/www/encolinks/run.py
ADD encolinks /var/www/encolinks/encolinks
ADD supervisord.conf /etc/supervisor/conf.d/encolinks.conf

RUN apt-get update
RUN apt-get install -y python python-pip supervisor
RUN pip install -r /var/www/encolinks/requirements.txt

RUN mkdir /var/www/encolinks/logs/
RUN touch /var/www/encolinks/logs/supervisor.log

EXPOSE 8000

CMD ["supervisord"]
